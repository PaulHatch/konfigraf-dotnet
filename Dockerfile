FROM mcr.microsoft.com/dotnet/core/sdk:2.2 AS base

WORKDIR /sln

# Copy the solution
ONBUILD COPY ./*.sln  ./

# Copy the main source project files
ONBUILD COPY src/*/*.csproj ./
ONBUILD RUN for file in $(ls *.csproj); do mkdir -p src/${file%.*}/ && mv $file src/${file%.*}/; done

# Restore packages in the base layer so they can be cached
ONBUILD RUN dotnet restore

FROM base as build
ARG VERSION
WORKDIR /sln 

COPY . .
RUN dotnet build /p:Version=$VERSION -c Release --no-restore 

FROM build as run
WORKDIR /sln

ENTRYPOINT ["sh", "/sln/build.sh"]


 
