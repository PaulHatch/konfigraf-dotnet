﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Konfigraf
{
    /// <summary>
    /// Represents a remote which when added to a repository will receive
    /// commits made to a configuration.
    /// </summary>
    public class ReplicationTarget
    {
        /// <summary>
        /// Initializes a new <see cref="ReplicationTarget"/> instance.
        /// </summary>
        /// <param name="url">The target URL.</param>
        /// <param name="username">The remote username.</param>
        /// <param name="token">The token or password to use when connecting.</param>
        public ReplicationTarget(Uri url, string username, string token)
        {
            Url = url;
            Username = username;
            Token = token;
        }

        internal Uri Url { get; }
        internal string Username { get; }
        internal string Token { get; }
    }
}
