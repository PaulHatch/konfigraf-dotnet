﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Konfigraf
{
    /// <summary>
    /// Represents the result of a list items operation.
    /// </summary>
    public class ListResult
    {
        /// <summary>
        /// Gets the names of the items for this result.
        /// </summary>
        [JsonProperty("items")]
        public IEnumerable<string> Items { get; internal set; }
    }
}
