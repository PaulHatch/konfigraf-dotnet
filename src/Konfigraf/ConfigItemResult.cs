﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Konfigraf
{
    /// <summary>
    /// The result of a request for a specific configuration item within a
    /// "get all" result.
    /// </summary>
    /// <typeparam name="TConfig">The type of the configuration object.</typeparam>
    public class ConfigItemResult<TConfig>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ConfigItemResult{TConfig}" /> class.
        /// </summary>
        /// <param name="config">The configuration value.</param>
        /// <param name="itemHash">The item hash.</param>
        public ConfigItemResult(
            TConfig config,
            string itemHash)
        {
            Value = config;
            ItemHash = itemHash;
        }

        /// <summary>
        /// Gets the value for this result.
        /// </summary>
        public TConfig Value { get; }

        /// <summary>
        /// Gets the hash for this item.
        /// </summary>
        public string ItemHash { get; }
        
    }
}
