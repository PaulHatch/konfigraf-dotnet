﻿using Newtonsoft.Json;

namespace Konfigraf
{
    /// <summary>
    /// Represents the result of a delete item operation.
    /// </summary>
    public class DeleteResult
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UpdateResult" /> class.
        /// </summary>
        /// <param name="repositoryHash">The new SHA-1 hash of updated repository.</param>
        public DeleteResult(string repositoryHash)
        {
            RepositoryHash = repositoryHash;
        }

        /// <summary>
        /// Gets or sets the new SHA-1 hash of updated repository.
        /// </summary>
        [JsonProperty("repoHash")]
        public string RepositoryHash { get; }
    }
}