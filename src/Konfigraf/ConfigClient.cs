﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Konfigraf
{
    /// <summary>
    /// This is the Konfigraf client class which can be used to update and retrieve
    /// configurations from a remote Konfigraf service.
    /// </summary>
    public class ConfigClient
    {
        private readonly Uri _host;
        private readonly IHttpClientFactory _clientFactory;
        private readonly HttpMessageInvoker _client;

        /// <summary>
        /// Initializes a new instance of the <see cref="ConfigClient"/> class. This
        /// version will use a default singleton HTTP client instance to communicate
        /// with the host.
        /// </summary>
        /// <param name="host">The service host URI.</param>
        public ConfigClient(Uri host)
        {
            _host = host;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ConfigClient" /> class. This
        /// version will use the provided HTTP client to communicate with the host.
        /// </summary>
        /// <param name="host">The service host URI.</param>
        /// <param name="httpClient">The HTTP client to use.</param>
        public ConfigClient(Uri host, HttpMessageInvoker httpClient)
        {
            _host = host;
            _client = httpClient;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ConfigClient" /> class. This
        /// version will use the provided HTTP client factory to create client instances
        /// to communicate with the host.
        /// </summary>
        /// <param name="host">The service host URI.</param>
        /// <param name="clientFactory">The client factory to use.</param>
        public ConfigClient(Uri host, IHttpClientFactory clientFactory)
        {
            _host = host;
            _clientFactory = clientFactory;
        }

        private HttpMessageInvoker Client => _client ?? _clientFactory?.CreateClient() ?? _defaultClient.Value;

        private static readonly Lazy<HttpClient> _defaultClient = new Lazy<HttpClient>(CreateClient);
        private static HttpClient CreateClient()
        {
            var handler = new SocketsHttpHandler
            {
                AutomaticDecompression =
                    DecompressionMethods.Deflate |
                    DecompressionMethods.GZip,
                SslOptions = new System.Net.Security.SslClientAuthenticationOptions
                {
                    RemoteCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true
                }
            };
            return new HttpClient(handler);
        }

        /// <summary>
        /// Gets or sets the global serializer to use for JSON serialization.
        /// </summary>
        public JsonSerializer Serializer { get; set; } = new JsonSerializer();

        /// <summary>
        /// Updates the configuration and returns the (git) hash of the newly
        /// created configuration.
        /// </summary>
        /// <typeparam name="TConfig">The type of the configuration file. if this
        /// type is a string content will be stored as raw text, if any other class,
        /// it will be serialized using JSON.</typeparam>
        /// <param name="configuration">The configuration settings to save.</param>
        /// <param name="repository">The configuration identifier, specifies a
        /// specific configuration repository, specify null to use a "global"
        /// configuration repository.</param>
        /// <param name="path">The path of the configuration file within the
        /// configuration repository.</param>
        /// <param name="message">Description of the update that was made.</param>
        /// <param name="user">The user responsible for this update.</param>
        /// <param name="email">The user's email address.</param>
        /// <param name="itemHash">The hash of the item/file being updated, if a value
        /// is provided the operation will fail if the specified hash is not the
        /// most recent for this item.</param>
        /// <param name="repoHash">The hash of the entire repository being updated,
        /// if a value is provided the operation will fail if the specified hash is
        /// not the most recent for this repository.</param>
        /// <param name="cancel">A cancellation token for this request.</param>
        /// <returns>
        /// The hash of the newly updated configuration.
        /// </returns>
        public async Task<UpdateResult> UpdateAsync<TConfig>(
                TConfig configuration,
                string repository,
                string path,
                string message = null,
                string user = null,
                string email = null,
                string itemHash = null,
                string repoHash = null,
                CancellationToken cancel = default)
        {
            var queryParams = new List<string>();
            if (!String.IsNullOrEmpty(user))
            {
                queryParams.Add($"usr={user}");
            }
            if (!String.IsNullOrEmpty(message))
            {
                queryParams.Add($"msg={message}");
            }
            if (!String.IsNullOrEmpty(message))
            {
                queryParams.Add($"eml={email}");
            }
            if (!String.IsNullOrEmpty(repoHash))
            {
                queryParams.Add($"itm={itemHash}");
            }
            if (!String.IsNullOrEmpty(itemHash))
            {
                queryParams.Add($"rep={repoHash}");
            }


            var requestUri = new Uri(_host, $"{repository}/i/{path}?{String.Join("&", queryParams)}");

            if (configuration is string text)
            {
                using (var content = new StringContent(text, Encoding.UTF8))
                using (var request = new HttpRequestMessage(HttpMethod.Post, requestUri) { Content = content })
                using (var response = await Client.SendAsync(request, cancel))
                {
                    response.EnsureSuccessStatusCode();
                    var body = await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<UpdateResult>(body);
                }
            }
            else
            {
                using (var ms = new MemoryStream())
                using (var sr = new StreamWriter(ms))
                using (var writer = new JsonTextWriter(sr))
                {
                    Serializer.Serialize(writer, configuration);
                    await writer.FlushAsync();
                    ms.Seek(0, SeekOrigin.Begin);

                    using (var content = new StreamContent(ms))
                    using (var request = new HttpRequestMessage(HttpMethod.Post, requestUri) { Content = content })
                    using (var response = await Client.SendAsync(request, cancel))
                    {
                        response.EnsureSuccessStatusCode();
                        var body = await response.Content.ReadAsStringAsync();
                        return JsonConvert.DeserializeObject<UpdateResult>(body);
                    }
                }
            }

        }

        /// <summary>
        /// Deletes a configuration item and returns the hash of the newly
        /// created configuration.
        /// </summary>
        /// <param name="repository">The configuration identifier, specifies a
        /// specific configuration repository, specify null to use a "global"
        /// configuration repository.</param>
        /// <param name="path">The path of the configuration file within the
        /// configuration repository.</param>
        /// <param name="message">Description of the update that was made.</param>
        /// <param name="user">The user responsible for this update.</param>
        /// <param name="email">The user's email address.</param>
        /// <param name="itemHash">The hash of the item/file being updated, if a value
        /// is provided the operation will fail if the specified hash is not the
        /// most recent for this item.</param>
        /// <param name="repoHash">The hash of the entire repository being updated,
        /// if a value is provided the operation will fail if the specified hash is
        /// not the most recent for this repository.</param>
        /// <param name="cancel">A cancellation token for this request.</param>
        /// <returns>
        /// The hash of the newly updated configuration.
        /// </returns>
        public async Task<DeleteResult> DeleteAsync(
                string repository,
                string path,
                string message = null,
                string user = null,
                string email = null,
                string itemHash = null,
                string repoHash = null,
                CancellationToken cancel = default)
        {
            var queryParams = new List<string>();
            if (!String.IsNullOrEmpty(user))
            {
                queryParams.Add($"usr={user}");
            }
            if (!String.IsNullOrEmpty(message))
            {
                queryParams.Add($"msg={message}");
            }
            if (!String.IsNullOrEmpty(message))
            {
                queryParams.Add($"eml={email}");
            }
            if (!String.IsNullOrEmpty(repoHash))
            {
                queryParams.Add($"itm={itemHash}");
            }
            if (!String.IsNullOrEmpty(itemHash))
            {
                queryParams.Add($"rep={repoHash}");
            }


            var requestUri = new Uri(_host, $"{repository}/i/{path}?{String.Join("&", queryParams)}");

            using (var request = new HttpRequestMessage(HttpMethod.Delete, requestUri))
            using (var response = await Client.SendAsync(request, cancel))
            {
                response.EnsureSuccessStatusCode();
                var body = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<DeleteResult>(body);
            }

        }

        /// <summary>
        /// Gets the raw string value of the specified configuration.
        /// </summary>
        /// <param name="repository">The repository identifier.</param>
        /// <param name="path">The path of the requested item.</param>
        /// <param name="cancel">A cancellation token for this request.</param>
        /// <returns>
        /// The requested configuration.
        /// </returns>
        public Task<Result<string>> GetAsync(
            string repository,
            string path,
            CancellationToken cancel = default) => GetAsync<string>(repository, path, cancel);

        /// <summary>
        /// Gets the specified configuration, deserializing to the specified result
        /// type using JSON if a type other than string is specified. If the requested
        /// path does not exist, the result will be null.
        /// </summary>
        /// <typeparam name="TConfig">The type of the configuration file. if this
        /// type is a string then raw content will be returned, if any other class,
        /// it will be deserialized using JSON.</typeparam>
        /// <param name="repository">The repository identifier.</param>
        /// <param name="path">The path of the requested file.</param>
        /// <param name="cancel">A cancellation token for this request.</param>
        /// <returns>
        /// The requested configuration.
        /// </returns>
        public async Task<Result<TConfig>> GetAsync<TConfig>(
            string repository,
            string path,
            CancellationToken cancel = default)
        {
            var requestUri = new Uri(_host, $"{repository}/i/{path}");

            using (var request = new HttpRequestMessage(HttpMethod.Get, requestUri))
            using (var response = await Client.SendAsync(request, cancel))
            {
                if (response.StatusCode == HttpStatusCode.NotFound)
                {
                    return null;
                }

                response.EnsureSuccessStatusCode();

                response.Headers.TryGetValues("X-RepoHash", out IEnumerable<string> RepoHash);

                var itemHash = response.Headers.GetValues("X-ItemHash").FirstOrDefault();
                var repositoryHash = response.Headers.GetValues("X-RepoHash").FirstOrDefault();
                
                object result;
                if (typeof(TConfig) == typeof(string))
                {
                    result = await response.Content.ReadAsStringAsync();
                }
                else
                {
                    using (var responseContent = await response.Content.ReadAsStreamAsync())
                    using (var reader = new StreamReader(responseContent))
                    using (var jsonReader = new JsonTextReader(reader))
                    {
                        result = Serializer.Deserialize<TConfig>(jsonReader);
                    }
                }

                return new Result<TConfig>(
                    (TConfig)result,
                    itemHash,
                    repositoryHash);
            }
        }

        /// <summary>
        /// Retrieves multiple configuration files from the specified path,
        /// deserializing to the specified result type using JSON if a type
        /// other than string is specified.
        /// </summary>
        /// <typeparam name="TConfig">The type of the configuration file. if this
        /// type is a string then raw content will be returned, if any other class,
        /// it will be deserialized using JSON.</typeparam>
        /// <param name="repository">The repository identifier.</param>
        /// <param name="path">The path of the requested file.</param>
        /// <param name="ifMatchHash">A repository hash to use for conditional
        /// requests. Like an etag, if this hash matches the current state of the
        /// repository an empty result will be returned with the <see cref="GetAllResult{TConfig}.NotModified"/> 
        /// property set to true.
        /// </param>
        /// <param name="cancel">A cancellation token for this request.</param>
        /// <returns>
        /// The requested configuration.
        /// </returns>
        public async Task<GetAllResult<TConfig>> GetAllAsync<TConfig>(
            string repository,
            string path,
            string ifMatchHash = default,
            CancellationToken cancel = default)
        {
            var queryParams = new List<string>();
            if (!String.IsNullOrEmpty(ifMatchHash))
            {
                queryParams.Add($"if-match={ifMatchHash}");
            }

            var requestUri = new Uri(_host, $"{repository}/i/{path.Trim('/')}/*?{String.Join("&", queryParams)}");

            using (var request = new HttpRequestMessage(HttpMethod.Get, requestUri))
            using (var response = await Client.SendAsync(request, cancel))
            {
                response.Headers.TryGetValues("X-RepoHash", out IEnumerable<string> repoHash);

                var results = new GetAllResult<TConfig>(repoHash?.FirstOrDefault(), response.StatusCode == HttpStatusCode.NotModified);
                if (response.StatusCode == HttpStatusCode.NoContent ||
                    response.StatusCode == HttpStatusCode.NotModified)
                {
                    return results;
                }

                response.EnsureSuccessStatusCode();

                var header = new byte[512];
                var itemIndex = 0;
                using (var content = await response.Content.ReadAsStreamAsync())
                {
                    while (content.Position + 512 + 512 < content.Length)
                    {
                        await content.ReadAsync(header, 0, 512);
                        var name = ReadString(header, 0, 100);
                        var type = (char)header[156];
                        Int32.TryParse(ReadString(header, 124, 12), out int size);

                        if (type != '0')
                        {
                            content.Position += size;
                        }
                        else
                        {
                            // Normal files

                            response.Headers.TryGetValues($"X-RepoHash-{itemIndex}", out IEnumerable<string> itemHash);
                            itemIndex++;

                            using (var fileStream = new SubStream(content, content.Position, size))
                            using (var reader = new StreamReader(fileStream))
                            {
                                TConfig fileResult;
                                if (typeof(TConfig) == typeof(string))
                                {
                                    fileResult = (TConfig)(object)reader.ReadToEndAsync();
                                }
                                else
                                {
                                    using (var jsonReader = new JsonTextReader(reader))
                                    {
                                        fileResult = Serializer.Deserialize<TConfig>(jsonReader);
                                    }
                                }
                                results.Items.Add(name, new ConfigItemResult<TConfig>(fileResult, itemHash?.FirstOrDefault()));
                            }
                        }

                        var pos = content.Position;
                        var offset = 512 - (pos % 512);
                        if (offset == 512)
                        {
                            offset = 0;
                        }

                        content.Seek(offset, SeekOrigin.Current);
                    }
                }

                return results;
            }

        }

        private static string ReadString(byte[] header, int start, int count)
        {
            for (int i = 0; i < count; i++)
            {
                if (header[start + i] == 0)
                {
                    return Encoding.ASCII.GetString(header, start, i);
                }
            }
            return Encoding.ASCII.GetString(header, start, count);
        }

        /// <summary>
        /// Gets differences in Git diff format for the specified file.
        /// </summary>
        /// <param name="repository">The repository identifier.</param>
        /// <param name="path">The path of the file to compare.</param>
        /// <param name="from">The start hash or tag of the version to compare,
        /// this value must not be null.</param>
        /// <param name="to">The end hash or tag of the version to compare if
        /// this value is null then the most recent version will be used.</param>
        /// <param name="cancel">A cancellation token for this request.</param>
        /// <returns>
        /// The diff for the requested file.
        /// </returns>
        public async Task<string> GetDiffAsync(
            string repository,
            string path,
            string from,
            string to = null,
            CancellationToken cancel = default)
        {
            var requestUri = new Uri(
                _host,
                $"{repository}/i/{path}?diff={from}" +
                (String.IsNullOrEmpty(to) ? String.Empty : "&to={to}"));

            using (var request = new HttpRequestMessage(HttpMethod.Get, requestUri))
            using (var response = await Client.SendAsync(request, cancel))
            {
                response.EnsureSuccessStatusCode();
                return await response.Content.ReadAsStringAsync();
            }
        }

        /// <summary>
        /// Gets a list of the items available at the specified path.
        /// </summary>
        /// <param name="repository">The repository identifier.</param>
        /// <param name="path">The path of the file to compare.</param>
        /// <param name="cancel">A cancellation token for this request.</param>
        /// <returns>
        /// A list of the available items.
        /// </returns>
        public async Task<ListResult> ListItemsAsync(
            string repository,
            string path,
            CancellationToken cancel = default)
        {
            var requestUri = new Uri(_host, $"{repository}/i/{path}?list=true");

            using (var request = new HttpRequestMessage(HttpMethod.Get, requestUri))
            using (var response = await Client.SendAsync(request, cancel))
            {
                if (response.StatusCode == HttpStatusCode.NotFound)
                {
                    return new ListResult { Items = Array.Empty<string>() };
                }

                response.EnsureSuccessStatusCode();
                var content = await response.Content.ReadAsStringAsync();

                return JsonConvert.DeserializeObject<ListResult>(content);
            }
        }

        /// <summary>
        /// Lists the available repositories.
        /// </summary>
        /// <param name="cancel">A cancellation token for this request.</param>
        /// <returns>
        /// A list of the available repositories.
        /// </returns>
        public async Task<RepositoriesResult> ListRepositoriesAsync(CancellationToken cancel = default)
        {
            var requestUri = new Uri(_host, "_list");

            using (var request = new HttpRequestMessage(HttpMethod.Get, requestUri))
            using (var response = await Client.SendAsync(request, cancel))
            {
                response.EnsureSuccessStatusCode();
                var content = await response.Content.ReadAsStringAsync();

                return JsonConvert.DeserializeObject<RepositoriesResult>(content);
            }
        }

        /// <summary>
        /// Creates a new repository. If the repository name already exists this method
        /// will return false.
        /// </summary>
        /// <param name="name">The repository name to create.</param>
        /// <param name="cancel">A cancellation token for this request.</param>
        /// <returns>True if the repository was created, otherwise false.</returns>
        public async Task<bool> CreateRepositoryAsync(string name, CancellationToken cancel = default)
        {
            var requestUri = new Uri(_host, name);

            using (var request = new HttpRequestMessage(HttpMethod.Post, requestUri))
            using (var response = await Client.SendAsync(request, cancel))
            {
                if (response.StatusCode == HttpStatusCode.Conflict)
                {
                    return false;
                }

                response.EnsureSuccessStatusCode();
                return true;
            }
        }

        /// <summary>
        /// Creates a new repository with the specified replication remote. If
        /// the repository name already exists this method will return false.
        /// </summary>
        /// <param name="name">The repository name to create.</param>
        /// <param name="remoteUrl">The replication remote URL.</param>
        /// <param name="username">The remote username.</param>
        /// <param name="token">The password token to authenticate the remote user.</param>
        /// <param name="cancel">A cancellation token for this request.</param>
        /// <returns>
        /// True if the repository was created, otherwise false.
        /// </returns>
        public async Task<bool> CreateRepositoryAsync(string name, string remoteUrl, string username, string token, CancellationToken cancel = default)
        {
            var requestUri = new Uri(_host, name);

            var requestBody = JsonConvert.SerializeObject(new { remote = remoteUrl, user = username, token = token });

            using (var content = new StringContent(requestBody))
            using (var request = new HttpRequestMessage(HttpMethod.Post, requestUri) { Content = content })
            using (var response = await Client.SendAsync(request, cancel))
            {
                if (response.StatusCode == HttpStatusCode.Conflict)
                {
                    return false;
                }

                response.EnsureSuccessStatusCode();
                return true;
            }
        }


        /// <summary>
        /// Deletes an existing repository. If the repository does not exist this method
        /// will return false.
        /// </summary>
        /// <param name="name">The name of the repository to delete.</param>
        /// <param name="cancel">A cancellation token for this request.</param>
        /// <returns>
        /// True if the repository was deleted, otherwise false.
        /// </returns>
        public async Task<bool> DeleteRepositoryAsync(string name, CancellationToken cancel = default)
        {
            var requestUri = new Uri(_host, name);

            using (var request = new HttpRequestMessage(HttpMethod.Delete, requestUri))
            using (var response = await Client.SendAsync(request, cancel))
            {
                if (response.StatusCode == HttpStatusCode.NotFound)
                {
                    return false;
                }

                response.EnsureSuccessStatusCode();
                return true;
            }
        }
    }
}
