﻿using Newtonsoft.Json;

namespace Konfigraf
{
    /// <summary>
    /// Represents the result of an update item operation.
    /// </summary>
    public class UpdateResult
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UpdateResult"/> class.
        /// </summary>
        /// <param name="itemHash">The SHA-1 hash of the item that was updated.</param>
        /// <param name="repositoryHash">The new SHA-1 hash of updated repository.</param>
        public UpdateResult(string itemHash, string repositoryHash)
        {
            ItemHash = itemHash;
            RepositoryHash = repositoryHash;
        }

        /// <summary>
        /// Gets or sets the SHA-1 hash of the item that was updated.
        /// </summary>
        [JsonProperty("itemHash")]
        public string ItemHash { get; }
        
        /// <summary>
        /// Gets or sets the new SHA-1 hash of updated repository.
        /// </summary>
        [JsonProperty("repoHash")]
        public string RepositoryHash { get; }
    }
}