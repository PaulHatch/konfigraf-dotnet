﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Konfigraf
{
    internal class SubStream : Stream
    {
        private readonly Stream _stream;
        private readonly long _offset;
        private readonly long _length;

        public SubStream(Stream stream, long offset, long length)
        {
            _stream = stream;
            _offset = offset;
            _length = length;
        }

        public override bool CanRead => _stream.CanRead;

        public override bool CanSeek => _stream.CanSeek;

        public override bool CanWrite => _stream.CanWrite;

        public override long Length => _length;

        public override long Position { get => _stream.Position - _offset; set => _stream.Position = value - _offset; }

        public override void Flush() => _stream.Flush();

        public override int Read(byte[] buffer, int offset, int count)
        {
            var readCount = Math.Min((int)(_length - Position), count);
            return _stream.Read(buffer, offset, readCount);
        }


        /// <summary>
        /// When overridden in a derived class, sets the position within the current stream.
        /// </summary>
        /// <param name="offset">A byte offset relative to the origin parameter.</param>
        /// <param name="origin">A value of type <see cref="T:System.IO.SeekOrigin"></see> indicating the reference point used to obtain the new position.</param>
        /// <returns>
        /// The new position within the current stream.
        /// </returns>
        public override long Seek(long offset, SeekOrigin origin)
        {
            switch (origin)
            {
                case SeekOrigin.Begin:
                    return _stream.Seek(offset + _offset, SeekOrigin.Begin) - _offset;
                case SeekOrigin.Current:
                    return _stream.Seek(offset, SeekOrigin.Current) - _offset;
                case SeekOrigin.End:
                    return _stream.Seek(offset + _offset + _length, SeekOrigin.Begin) - _offset;
                default:
                    throw new ArgumentException(nameof(origin));
            }
        }

        public override void SetLength(long value)
        {
            throw new NotImplementedException();
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            throw new NotImplementedException();
        }
    }
}
