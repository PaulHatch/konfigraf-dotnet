﻿namespace Konfigraf
{
    /// <summary>
    /// Represents information about a repository.
    /// </summary>
    public class RepositoryResult
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RepositoryResult"/> class.
        /// </summary>
        /// <param name="name">The name of the repository.</param>
        public RepositoryResult(string name)
        {
            Name = name;
        }

        /// <summary>
        /// Gets the name of the repository.
        /// </summary>
        public string Name { get; }
    }
}