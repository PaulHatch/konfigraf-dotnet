﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Konfigraf
{
    /// <summary>
    /// Result of a "get all" items request.
    /// </summary>
    /// <typeparam name="TConfig">The type of the configuration object.</typeparam>
    public class GetAllResult<TConfig>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GetAllResult{TConfig}"/> class.
        /// </summary>
        /// <param name="repoHash">The repository hash.</param>
        /// <param name="notModified">The not modified status.</param>
        public GetAllResult(string repoHash, bool notModified)
        {
            RepoHash = repoHash;
            NotModified = notModified;
        }

        /// <summary>
        /// Gets a value indicating whether the result has was not returned due to
        /// not having been modified from the repository hash provided.
        /// </summary>
        public bool NotModified { get; }

        /// <summary>
        /// Gets the repository hash.
        /// </summary>
        public string RepoHash { get; }

        /// <summary>
        /// Gets the items in the result.
        /// </summary>
        public IDictionary<string, ConfigItemResult<TConfig>> Items { get; }
            = new Dictionary<string, ConfigItemResult<TConfig>>();
    }
}
