﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Konfigraf
{
    /// <summary>
    /// The result of a request for a specific configuration item.
    /// </summary>
    /// <typeparam name="TConfig">The type of the configuration object.</typeparam>
    public class Result<TConfig>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Result{TConfig}" /> class.
        /// </summary>
        /// <param name="config">The configuration value.</param>
        /// <param name="itemHash">The item hash.</param>
        /// <param name="repoHash">The repository hash.</param>
        public Result(
            TConfig config,
            string itemHash,
            string repoHash)
        {
            Value = config;
            ItemHash = itemHash;
            RepoHash = repoHash;
        }

        /// <summary>
        /// Gets the value for this result.
        /// </summary>
        public TConfig Value { get; }

        /// <summary>
        /// Gets the hash for this item.
        /// </summary>
        public string ItemHash { get; }

        /// <summary>
        /// Gets the repository hash.
        /// </summary>
        public string RepoHash { get; }
    }
}
