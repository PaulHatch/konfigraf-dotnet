﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Konfigraf
{
    /// <summary>
    /// The result of a repository list request.
    /// </summary>
    public class RepositoriesResult
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RepositoriesResult"/> class.
        /// </summary>
        /// <param name="repositories">The resulting repositories.</param>
        public RepositoriesResult(IEnumerable<RepositoryResult> repositories)
        {
            Repositories = repositories;
        }

        /// <summary>
        /// Gets a list of the available repositories.
        /// </summary>
        public IEnumerable<RepositoryResult> Repositories { get; }
    }
}
