using System;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace Konfigraf.Tests
{
    public class IntegrationTest
    {
        private const string _repo = "testrepo";

        private class Config
        {
            public int MaxValue { get; set; }
        }

        [Fact]
        public async Task CreateRepo()
        {
            var client = new ConfigClient(new Uri("http://localhost:7003"));

            var preList = await client.ListRepositoriesAsync();
            await client.CreateRepositoryAsync(_repo);

            await client.UpdateAsync(new Config { MaxValue = 10001 }, _repo, "config/value1.json");
            await client.UpdateAsync(new Config { MaxValue = 2 }, _repo, "config/value2.json");
            await client.UpdateAsync(new Config { MaxValue = 3 }, _repo, "config/value3.json");
            await client.UpdateAsync(new Config { MaxValue = 3 }, _repo, "config/t/value3.json");

            await client.GetAllAsync<Config>(_repo, "config");

            await client.GetAllAsync<Config>(_repo, "none");


            var results = await client.GetAllAsync<Config>(_repo, "config");

            var notModifiedResult = await client.GetAllAsync<Config>(_repo, "config", results.RepoHash);

            var list = await client.ListRepositoriesAsync();
            await client.DeleteRepositoryAsync(_repo);
            var postList = await client.ListRepositoriesAsync();

            Assert.Equal(4, results.Items.Count());
            Assert.False(results.NotModified);
            Assert.Contains(results.Items.Keys, v => v == "value1.json");
            Assert.Contains(results.Items.Keys, v => v == "value2.json");
            Assert.Contains(results.Items.Keys, v => v == "value3.json");
            Assert.Contains(results.Items.Keys, v => v == "t/value3.json");
            Assert.Equal(10001, results.Items["value1.json"].Value.MaxValue);
            Assert.Equal(2, results.Items["value2.json"].Value.MaxValue);
            Assert.Equal(3, results.Items["value3.json"].Value.MaxValue);
            Assert.Equal(3, results.Items["t/value3.json"].Value.MaxValue);

            Assert.True(notModifiedResult.NotModified);
            Assert.Empty(notModifiedResult.Items);

            Assert.DoesNotContain(preList.Repositories, v => v.Name == _repo);
            Assert.Contains(list.Repositories, v => v.Name == _repo);
            Assert.DoesNotContain(postList.Repositories, v => v.Name == _repo);
        }
    }
}
